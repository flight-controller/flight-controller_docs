# Flight controller documentation repository

This repository contains all the documents regarding the [flight-controller](https://gitlab.com/flight-controller/flight-controller.git) project. The repository is divided into as follows.

- [Datasheets](https://gitlab.com/flight-controller/flight-controller_docs/-/tree/main/Datasheets?ref_type=heads)
- [Plantuml](https://gitlab.com/flight-controller/flight-controller_docs/-/tree/main/Plantuml?ref_type=heads)
- [Design_Guides](https://gitlab.com/flight-controller/flight-controller_docs/-/tree/main/Design_Guides?ref_type=heads)

---

## [Datasheets](https://gitlab.com/flight-controller/flight-controller_docs/-/tree/main/Datasheets?ref_type=heads)

Here you will find all the handy datasheets from the chip manufactures regarding the microcontroller, sensors and other ICs used in the [flight-controller](https://gitlab.com/flight-controller/flight-controller.git) project.

## [Plantuml](https://gitlab.com/flight-controller/flight-controller_docs/-/tree/main/Plantuml?ref_type=heads)

Here you will find all the design diagram that have been used for the flight-controller firmware development. It may contain UML diagrams or other methods of expressing designs.

## [Design_Guides](https://gitlab.com/flight-controller/flight-controller_docs/-/tree/main/Design_Guides?ref_type=heads)

Here you will find all the design guidelines that manufacture provides regarding the chips that have been used in the flight-controller project.
